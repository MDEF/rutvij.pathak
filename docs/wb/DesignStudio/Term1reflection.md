
# Term 1 Reflection

## The project is bigger than the devices themselves.

### Design Considerations : -

Every Design consideration which needs to be made will be infact made for the ecosystem i.e these devices need to exist in .

For eg :  Attiny 44 is used in the design because it is the most commonly found chip in the fablabs across the Fab-Lab Network.  

The batteries which will be used in the system will be 9V chip-ons or 3.3V AA batteries. This is also considering the cost and availability of the batteries.

Every Design consideration to be made will be to existing resources of the eco-system of the field that it exists.

### Overall Considerations : -

This is the gist of the reviews I got from the session . The first revelation after various chats was that these devices cannot exist  in a vacuum on their own.  The ecosystem of these various  projects that have existed before and why have they failed.

The following analysis of the projects has been done and catalogued.

1. HeartyPatch  - Company develops the patch as the product and not the
2. OpenECG   -  Similar to HeartyPatch.

The various comment of the Jury include as quoted  :

The photo represents :

This was the desk setup for the first term presentation, I explored various options of presenting flat pack deployable devices . When flat pack come to mind so does IKEA . I explored it and presented it in terms of cardboard prototypes which are presented in term . The comments that I got from various jury members were as follows

1. Marianna's Comments-
2. Tomas's Comments : Thinking about the system of adoption and also the ecosystem in which the products exist
3. Guiillem's Comments - It was thinking about the supply chains which will be needed to setup
4. Santi's Comments - It was the real world experience of the project rather than idealized lab version.
5.



My Reflections on the First Term - A lot of work has be done in terms of the narrative and how the project is presented to stakeholders and possible partners. This project is not just about the device but also equally important the ecosystem in which this project exists.

Understanding the ecosystem of the people that are going to use this , the organizations which will be using/financing this . The eco-system of the makers who will be designing the open source designs.

Future direction of the Project :

1. Find Potential Collaborators
2. Find the Supply Chains which currently exist.

Plan for the Term 2 : -

Get into prototyping look into the how the design will be fabricated in the fablabs. The main 3 pillars of the design will be the machines available in the labs , the materials available in labs and skills of the individuals in the labs.
