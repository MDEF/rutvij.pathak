#Shenzhen Research Trip


 ![](wb\ResearchTrip\buildings.jpg)

_Shenzen , China_

##Shenzen - Silicon Valley of Hardware

- Shenzhen Experience was from a perspective of an electronics enthusiast .Shenzhen is filled with various wonderful oddities that live and breathe in their ecosystem. This was fully enriched with designers, Engineers, Entrepreneurs . This was the system where you could get, the most obscure and detailed things such as switches with specific feels. This was the first impression of Huaquanbeie, the electronics market, . Over the next couple of trips to the market , we discovered that farther go get from the main street , more obscure and unfinished the products become ,this was the trend , the main street represented mostly finished products ( whitelabel ) and further you went away from the market you could see  the product into its constituent parts . For eg : A computer Tablet on the main street could be built to full custom configuration in one of the adjacent buildings .
![](wb/ResearchTrip/shenzhenmarket.jpg)
_The famous Huaquanbeie market_

##The Shift of Shenzen Culture

- There was also talks of evolving Shenzhen , where there were talks of Shenzen evolving over a period of 10 years , going from just a Manufacturing hub to adding designing Hub along the way , Right now Shenzhen has designing capabilities , with 150 thousand industrial designers . Designing entire full fledged projects to providing manufacturing design assistance to existing designers across the world .
SEEED Studio was one of the most intresting places to visit in the entire Shenzhen Trip as they are at the place between East and West . They are leveraging their product website to pivot into a platform which can provide turn-key solutions for small startups or enthusiast trying to get into product development .
David Li  gave a really great talk about the spirit of Shenzhen and how this culture of Shenzhen and other parts of China which are following similar models for the TaoBao village where they have started to come up with golf carts which then morphed into small capacity/capability of electric vehicles which now exists in the legal grey area as people are using them though the government has not allowed them on the street . But these vehicles are generating lot of employment and commerce for the village , thus being a viable of source of income and industry for the local economy there . This reflects the quick attitude of the Chinese government .

##SEEED Studio
![](wb\ResearchTrip\seeedstudio.jpg)
_Presentation by Eric Pan , CEO of SEEEDStudio_
- We visited the SEEED Studion HQ and x.factory , factory of SEEED Studio , they are extremely open to the idea of integrating the external the western designers , small firms and the connect them to the manufacturing infrastructure of Shenzhen .  

##Analysis
- Analysing the whole trip in context of my project . Shenzhen seems to be the perfect playground for people with an idea to test various ideas quickly and figure out what is working and what isn’t. Shenzhen also offers the possibility of using the existing ecosystem to build on existing ecosystem where existing products exist and could be mutated into something that could be used for the project instead of building everything from the ground up . Existing infrastructure of people and machine on building a project together .Shenzhen provides a very rich environment for innovation and development . This is also the charm of Shenzhen . Shenzhen is as much the network of people in it as the machinery and the infrastructure of it .
