#09.

##Engaging Narratives
- 26/ 11 –  Discussions with Tomas test12
- 27/11 –  Kickstarter Presentation by Heather Corcoran
- 28 /11 – Kickstarter Presentation by Heather Corcoran
- 29 /11 –   Workshop on Bjarke Calvin
- 30/11 –  Final day of presentation

###Reflections:
- The discussion on Monday with presented with various different questions and probes into the project which resulted in things that I wanted to explore for example, pitching the project from a different perspective for it to be more appealing.
- Heather Corcoran:
   - This was an introduction for the Kickstarter/crowdfunding scenarios in the real world, the statistics and overview of the design and technology projects.
-  We had to come up with a title, subtitle, and brief project description,
Heather discussed with the framing of our projects, the format of a crowd sourcing campaign with
Various strategies in doing awareness, the types of rewards that people respond to,
How a opensource project can be monetized which was super helpful.

- Bjarke talked about the essentials of storytelling and documentary filmmaking which essentially are used to design engaging the stories also the we tried the app , Duckling which is useful for creating stories .

The following is the one I came up with,
###XYZ – Open Source and Modular family of healthcare and diagnostic devices.  
###Subtitle – XYZ is an open source family of diagnostic devices, which is affordable, accessible, and customizable which allows you to take control of your own data.
- Product Shot  
![](week9-v1.jpg)

Photoshot  of the next page needs to be added .


During the presentation with Marianna we discussed with the possibility of the product literacy , i.e among doctors and patients . The information thread that each person extracts from the device , such as indicators and displays of various things.  

![](week9-v2.jpg)
- This sketch tentatively shows the family of devices that will be executed during course of the the masters ,

This week has been superhelpful,in providing new insights into my projects .
