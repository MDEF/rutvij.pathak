#04.

##  Exploring Hybrid Profiles in Design

##LOG OF THE WEEK  
- OCT 22 :  Visit to Domestic Data Streamers."
- OCT 23 :  Visit to Ariel Guersenzvaig at Elisava "

- OCT 17:  Visit to Sar at La vieda
- OCT 18:  Reflections with Oscar - Personal Development Plan "
- OCT 19:  Reflections with Oscar - Project Plan "

## Personal Notes

1. Personal reflections on  the designers .

-	a. Domestic Data Streamers – The ability to extract raw emotion from the simple and  everyday mundane things was extraordinary. Their use of making things available for people to express themselves i.e creating various avenues for their expression and interaction .
-	b .Areil (Elisava) – I appreciated his sheer vastness in knowledge . His use of Ethics in Business design . and the impact of design on Society are something I would also like to keep in mind when I’m designing by own project.
-	c. Sara ( La Vieda )  - Her practical knowledge about various materials and its uses . I liked how she used unconventional materials to prototype and make shoes out of it . I also like how she respected and used the traditional craftsmanship of shoemaking into her knowledge base .

2. Personal reflections on  the my skills currently .

![](week4-1.png)

2. Personal plan to acquire knowledge in the first trimester .

![](week4-2.png)


##Following is the direction I want to take my project

###Prediction via personal experience

1.	Place I worked ( Vigyan Ashram ) . The apparent need of healthcare solutions
2.	India is an emerging market for companies like Google (Google Go) , Siemens . Apple is getting into healthcare .
3.	Same reason as the paper of Oscar “Designing for , with or within 1st , 2nd and 3rd  person points of view on designing for systems .”

###A goal

-	To design tools and devices (tangible ) on the field .

###A path to follow

-	Deploy with Doctors without Borders
-	Red Cross
-	UN ? (Working with the SDG goals )

###Driving force

-	The “ need ” for it which is apparent

###Scenario

-	Current scenario will be basic stats monitoring

###Inspiration
-	1. Medicine delivery drone for Dassault Lab
![](foldoscope.jpg)
source :https://www.foldscope.com/
-	2. Fold-o-scope
![](Otherlab_SkyMachines_APSARA-696x391.jpg)
source : https://otherlab.com/projects


###Commitment

-	The next 5 years

###A framework

-	Mission Critical , Health Stats to be monitored not getting into pathology
-	(Tools to help doctors on the field )
