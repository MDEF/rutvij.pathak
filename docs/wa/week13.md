#01. MDEF BOOTCAMP

##LOG OF THE WEEK  
- OCT 1:  Presentation about the masters and the future of the program by Tomas Diez "
- OCT 2:  Introductions of peers and group formation based on mutual interest for peer -to - peer collaboration "

- OCT 3:  GIT Tutorial by Santi and Xavi "
        - New things learnt about  MKDOCS
                                   ATOM
- OCT 4:  Presentation by Tomas Diez + City Safari + Scavenging"
- OCT 5:  Presentation by Oscar Tomico and his body of work. + Reflection excercise  by Marianna  "

------------------------------------------------------
###  What did you do that is new. What did you learn?
- I learnt about MKDOCS i.e. markdown documents which can be converted into HTML . I found  MKDOCS to be quite easy to handle . It's much easier than maintaining all the  tags in HTML coding .
- Being resourceful is a crucial part of the circular economy and sustainability. Some of the main categories of things that we were told to be on the lookout for were
           1. Flat Surfaces - This includes tabletops , Hardwood which will be used for CNC feed .
           2. Parts of a whole - Power supplies of old machines , motors , fans which can be repurposed into new machines .
           3. Plastics - Acrylic / plexiglass for bending and cutting .
- There are lots of "weak signals" that we as designers need to be on the  lookout for . This means that there is a latent need for it yet nobody has expressed it .
- There are various hidden networks of complexity that often drive economies/people that also need to be taken into consideration when designing .

### How can your learning process be described?
1.  Assimilation i.e. just listening to the lecture and noting down key points and ideas .
2.  Researching the key ideas and related lateral ideas .
3.  Taking 2nd set of notes which will be a with some context for some of the key points discussed .
4.  Confirming my concept are correct .  
5.  Application of the concepts in design / practice


### How is this relevant to your work?
- I want to work in the field of biomedical instrumentation which will be related to wearables .
- Oscars explained the how explicit of design is used to create new experiences such as Vibration motors .
- Solution must has desirability , expressiveness , identity .
- As designers we need to design for the real context i.e. real products cannot be  designed by sitting in offices . Experiencing the real world conditions is of the utmost importance .
- Study of the textures must be done i.e. how certain texture makes you feel.
------------------------------------------------------
### Reading list for this week -

  1. Century of the self - Alan Cortiz  
  2. Atlas of economic complexity - MIT
------------------------------------------------------

### Personal Reflection points and thoughts of the week  
 1. Sweet spot of the product exists between the want and the need of the consumer/user.
 2. Narrowing and then widening of the context which is the point of inflection that change can be introduced into the system.
 3. When designing a wearable /product aspect of "feel" of the product needs to be taken into consideration.
