
#Design for New

##Reflections 
The class was about understanding that the interventions don't exist in the vaccum and standalone things . They exist in a practise. My practice of exploration is " self-medicine ".  I explored in the aspect of the skills , images and stuff.

Following illustration below stuff , images and skills triangle.  
![](DesignForNew-01.png)
*This is the present map of Personal Wellbeing*


##CURRENT PRACTICES

Currently the whole paradigm of healthcare is looked on looked as a luxury these days. The image above illustrates the triangle of skills , stuff and images.
###SKILLS

_Self medicine_ is the non - emergency and non- serious grievance which is usually a cold or small infections.
_Doctor's Skill_ This is huge factor in the diagnosis of today's experience is the skill of the doctor which might lead to incorrect and potentially fatal diagnosis.
_Self Tracking_ This skill is how the patient is in tune with his own body.and knows that something is wrong.

###STUFF

_MedicalDevices_ These can be implanted or wearable i.e. artificial hip joints , pacemakers , insulin pump to a blood pressure monitor.
_Clinical Supplies_ Basic Disinfecting gear used to sterilize the patients body or clinic area.
_Auxiliary Medical Furniture_ Basic tables , chairs etc.


![](DesignForNew-02.png)
*This is the future map of Personal Wellbeing*

![](DesignForNew-03.png)
*This is the overall transistion towards distributed Healthcare and greater access to healthcare.*

This became the basis for my exploration. In it was also about understanding the relationships between these variables. These variables for the project turned into an overall exploration for the direction of my Master's Project.

This allowed me to explore the broader exploration of self-care this allowed me

The full design actions for my Master's Project of "HelloHealth" is
![](DesignForNew-04.png)
**
