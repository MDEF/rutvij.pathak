# Emergent Business Models

##Reflections:

This class was about emergent business models  This was about understanding the emerging ways of doing business . Understanding on the way the businesses operate .

It was about understanding the various gears and lever inside the system of a business which make it run smoothly.

The class was divided into 2 sessions

Session 1 - was about understanding the innovations :  agents , functions and relationships. It was basically deconstructing the complex ecosystem surrounding the intervention.

In this session there was about understanding the expanded ecosystem of the impact. i.e the  agents involved in this game .When saying this I mean it the abstract sense of the game.

This expanded impact ecosystem is made of 6 factors i.e. -

1. People  - Users , Manufacturers , Policy makers , etc
2. Knowledge - Knowledge which is generated through data.
3. Spaces - Spaces where the intervention exists i.e Offices or Public Spaces etc.
4. Funding - Who is funding in this field , etc who can possibly fund this intervention .
5. Distribution - Also called dissemination of the project that also means its adoption by the users.
6. Value Creation - What kind of value can be created by this project/intervention for its users etc.

- This boils down the wicked problem into these different spectrums though which the problem can be viewed and solved.After the session we had to include this model in our own project which was quite usefull.

![](Session1-Rutvij.png)
*Example of the slide that were used to analyze the existing projects*


I analyzed various projects along the line of

a. What type of solution is proposed ?
= Is it platform , product or a service.

b. What elements have been changed ?
= The recipe of factors used to generate the impact.

c.  What was the key leveraging point for this project ?
=  Most critical leveraging part of the project.

d.  What adjacent system has this innovation affected ?
= Intended/Unintended consequence of the system.

e.  What external as well as internal efforts has this innovation minimized ?
= What needs  of the latent needs of the users have been met and exceeded.

##Analysis

I analyzed various projects along these parameters and noted down various observations they are below as follows :

1. HeartyPatch : Open Source design of an ECG using clinical grade chips .The open source design makes it easier for people to use and replicate. This project started on [Hackaday.com](http://hackaday.com) but it didnt spread out from that site. The device itself is quite complicated to make/replicate without advanced tools and machinery

2. OpenBCI :  Open BCI ( Brain Computer Interfaces ) - Highly Advanced Open Source toolkit for Laboratory Research. It is complicated for the average novice user to replicate/use these boards.

3.  OpenAPS :  OpenAPS became a community of patients which came together to form a   solution for the problems they were having with the glucose monitoring systems they were using.The biggest key leveraging point of this project is the community which was built around people which it uses to continuously innovate.The community lacks the resources to design a solution instead hacking something together.

####Overall Reflections about the projects :

My intervention will need to have to include various things in order for it to work . it needs to have strong technical community  as well it needs to have a strong user base which will understand how the devices can be used.

The key points I have chosen to pick from the projects are

1. Open Source nature of HeartyPatch -  Open Sourcing the designs allows more number of people to jump on board the project and can help out with various aspects of the projects.
2. Community aspect of OpenAPS. -  Using a big community of users help in gaining readback for the designs that are designed by the technical community.
3. Development and Research aspect of OpenBCI-  There needs to be a roadmap for development of the project and how it can be made better. OpenBCI tackling this by involving universities and other research institutes which can be to make the project better.

- We had to fill a newspaper template which led to a bunch of direct questions which were useful to distill the ideas into proper.  Embedded below is the page for the newspaper that we had write.

![](Session2-Rutvij.png)
*This is the newspaper that really helped to clarify the intervention*

##RoadMap for HelloHealth: -

- I have decided to use all the aspects of the projects which will be incorporated in "HelloHealth".

- Open Source design , strong community oriented approach and modularity to maximize the impact of this project.

- This will be done by using the fabacademy network to leverage the technical users/designers. Using wikifactory as a main node for organizing the community around it and maintaining files and documentation at a central repository.  

- The NGO's will be useful in getting these devices to users as quickly as possibly as there is already a people network which is already existing in these areas.
